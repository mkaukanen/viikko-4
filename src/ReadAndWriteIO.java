
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Miki
 */
public class ReadAndWriteIO {
    
    /*public String readFile(String s) throws FileNotFoundException, IOException{
        BufferedReader in = new BufferedReader (new FileReader(s));
        String line;
        StringBuilder filetext = new StringBuilder();
        line = in.readLine();
        while (line != null){
            filetext.append(line);
            line = in.readLine();
            filetext.append("\n");
        } 
        in.close();
        // get the last line break off
        filetext.setLength(filetext.length()-1);
        String text = filetext.toString();
        return text;  
    }*/
    
    public void readAndWrite(String s, String s2) throws IOException{
        BufferedReader in = new BufferedReader (new FileReader(s));
        String line;
        StringBuilder filetext = new StringBuilder();
        line = in.readLine();
        while (line != null){
            if (line.length() < 30 && line.replace(" ", "").length() != 0 && line.contains(("v"))){
            filetext.append(line);
            filetext.append("\n");
            }
            line = in.readLine();
        } 
        in.close();
        // get the last line break off
        filetext.setLength(filetext.length()-1);
        String text = filetext.toString();
        
        BufferedWriter out = new BufferedWriter (new FileWriter(s2));
        out.write(text +"\n");
        out.close();
    }
}
